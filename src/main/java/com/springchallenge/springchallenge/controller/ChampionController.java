package com.springchallenge.springchallenge.controller;

import com.springchallenge.springchallenge.entities.LolChampion;
import com.springchallenge.springchallenge.service.ChampionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/lolchampion")
public class ChampionController {

    private static final Logger LOG = LoggerFactory.getLogger(ChampionController.class);


    @Autowired
    ChampionService championService;

    @GetMapping
    public List<LolChampion> findALl(){
        return this.championService.findAll();
    }

    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion){
        LOG.debug("Request to create champion ["+lolChampion+"]");
        return this.championService.save(lolChampion);
    }

}
