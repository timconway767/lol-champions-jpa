package com.springchallenge.springchallenge.service;

import com.springchallenge.springchallenge.entities.LolChampion;
import com.springchallenge.springchallenge.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChampionService {

    @Autowired
    LolChampionRepository championRepository;

    public List<LolChampion> findAll(){
        return championRepository.findAll();
    }
    public LolChampion save(LolChampion lolChampion){

        return championRepository.save(lolChampion);

    }
}
