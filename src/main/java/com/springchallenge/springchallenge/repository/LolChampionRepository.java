package com.springchallenge.springchallenge.repository;

import com.springchallenge.springchallenge.entities.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LolChampionRepository extends JpaRepository<LolChampion, Long> {
}
